FROM ubuntu:14.04

MAINTAINER Gronpipmaster

# env values
ENV TIMEZONE Asia/Novosibirsk

RUN locale-gen en_US en_US.UTF-8
ENV LANG en_US.UTF-8

# Update the base system with latest patches
RUN apt-get update -y && apt-get upgrade -y

RUN apt-get install software-properties-common -y && \
    apt-add-repository ppa:ondrej/php5 -y && \
    apt-get update -y

# Install PHP and its extensions
RUN apt-get install -y --force-yes php5-fpm php5-cli \
    php5-dev php-pear php5-apcu php5-json php5-mcrypt php5-curl \
    php5-mongo php5-mysql php5-redis php5-sqlite \
    php5-intl php5-imagick php5-gd

# Install nginx
RUN apt-get install -y --force-yes nginx

# Install utils
RUN apt-get install -y --force-yes curl

# apt clean
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/*

# Set timezone
RUN echo $TIMEZONE > /etc/timezone && dpkg-reconfigure --frontend noninteractive tzdata
RUN sed -i "s@^;date.timezone =.*@date.timezone = $TIMEZONE@" /etc/php5/*/php.ini

# Configure nginx
RUN rm /etc/nginx/sites-enabled/default && \
    echo "\ndaemon off;" >> /etc/nginx/nginx.conf
ADD etc/nginx/conf.d/vhost.conf /etc/nginx/conf.d/default.conf

# Import script used to launch PHP FPM and nginx
ADD etc/init.d/php-nginx /etc/init.d/php-nginx

RUN chmod +x /etc/init.d/php-nginx
RUN update-rc.d php-nginx defaults

# Expose ports
EXPOSE 80

CMD /bin/bash -c 'service php-nginx start'
