Test dockerfile
===============

### Development
#### Follow installation instructions for your distribution
[https://docs.docker.com/installation](https://docs.docker.com/installation)
#### Install composer
```bash
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin && sudo mv /usr/local/bin/composer.phar /usr/local/bin/composer
```
#### Clone project repository, build and run docker contaner
```bash
git clone git@github.com:/user/project ./
./build.sh
```

### Manual run docker container
```bash
#build image
sudo docker build -t some_image_name .

#run container
sudo docker run -d -P \
    --name="test" \
    -v /var/www/test:/var/www/test \
    some_image_name
#or
sudo docker run -d -p 5000:80 \
    --name="test" \
    -v /var/www/test:/var/www/test \
    some_image_name

#stop container
sudo docker stop test

#show list all containers
sudo docker ps

#remove container
sudo docker rm test
```
