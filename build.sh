#!/bin/bash

readonly CURRENT_DIR=$(pwd)
readonly IMAGE_NAME='test'
readonly CONTAINER_NAME='example'
readonly APP_DIR=$CURRENT_DIR/app

setPermissions(){
    # Set logs permissions
    mkdir -p $APP_DIR/logs
    chmod go+wx $APP_DIR/logs
    # Set cache permissions
    mkdir -p $APP_DIR/cache
    chmod go+wx $APP_DIR/cache
}

installDependency(){
    echo "[`date +%F--%T`] --- Install dependency ---"
    cd $CURRENT_DIR
    composer install || return 1
    return 0
}

createImage(){
    echo "[`date +%F--%T`] --- Create docker image ---"
    cd $CURRENT_DIR
    sudo docker build -t $IMAGE_NAME . || return 1
    return 0
}

runContainer(){
    echo "[`date +%F--%T`] --- Run docker container ---"
    sudo docker run -d -p 5000:80 \
        --name $CONTAINER_NAME \
        -v $CURRENT_DIR:/var/www \
        $IMAGE_NAME || return 1
    return 0
}

showContaner(){
    echo "[`date +%F--%T`] --- Show docker container ---"
    sudo docker ps | grep $CONTAINER_NAME || return 1
    return 0
}

main(){
    setPermissions
    installDependency || return 1
    createImage || return 1
    runContainer || return 1
    showContaner || return 1
    echo "[`date +%F--%T`] --- Success ---"
    return 0
}

main || exit 1
